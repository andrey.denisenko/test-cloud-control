terraform init -input=false
terraform apply --auto-approve -input=false -var="image=${CC_IMAGE_NAME}" -var="version_tag=${CC_VERSION_TAG}"
