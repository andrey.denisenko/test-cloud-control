output "public_url" {
  description = "Beanstalk Public URL"
  value       = join("", concat(["https://"], aws_elastic_beanstalk_environment.env.*.cname))
}