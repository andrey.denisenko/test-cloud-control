terraform {
  required_version = ">= 0.13"
}

provider "aws" {
  region = var.region
}

resource "random_string" "random" {
  length  = 6
  special = false
  upper   = false
}

locals {
  prefix = random_string.random.result
}
        
    terraform {
      required_providers {
        tls = {
          source  = "hashicorp/tls"
          version = "3.4.0"
        }
      }
    }
    