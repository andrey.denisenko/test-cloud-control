function build_module {
  dir=$1
  name=$( echo "$dir" | sed -e 's#\.\/##' -e 's#\/#-#g' )
  echo "Building module $name ($dir)..."
  mkdir -p ./build/terraform/$name
  terraform-config-inspect --json $dir > ./build/terraform/$name/config.json

  dep_modules_json=$(cat ./build/terraform/$name/config.json | \
    jq -r '[.module_calls[] | select(.source | test("^[.]{1,2}\/"; "s"))] | reduce .[] as $i ({}; .[$i.name] = $i.source)')

  IFS=$'\n' readarray -t dep_module_names < <( echo "$dep_modules_json" | jq -r 'to_entries[].key' )
  for dep_module in "${dep_module_names[@]}"
  do
    dep_relative_path=$(echo $dep_modules_json | jq -r --arg MODULE "$dep_module" '.[$MODULE]')
    dep_absolute_path=$(cd $dir; cd $dep_relative_path; pwd)
    dep_path=$(realpath --relative-to=$(pwd) "$dep_absolute_path")
    dep_name=$(echo "$dep_path" | sed 's#\/#-#g')
    echo "Detected module $dep_module with dependency to local module $dep_name ($dep_path)"
    echo "module \"$dep_module\" {
      source = \"\"
    }" > $dir/module_$dep_module_override.tf

  done
}

echo "Detecting Terraform modules..."
IFS=$'\n' readarray -t dirs < <( find . -type f -name '*.tf' -not -path '*.terraform*' | sed 's#\(.*\)/.*#\1#' | sort -u)
echo "Detected Terraform modules locations:"
printf '%s\n' "${dirs[@]}"

for dir in "${dirs[@]}"
do
  build_module "$dir"
done